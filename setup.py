from setuptools import setup

setup(
    name = "Sandastray",
    version = "1.0.0",
    options = {
        "build_apps" : {
            "include_patterns" : [
                "**/*.png",
                "**/*.ogg",
                "**/*.txt",
                "**/*.bam",
                "**/*.wav",
                "**/*.otf",
                "**/*.vert",
                "**/*.frag",
                "**/*.config"
            ],
			"exclude_patterns" : [
                "build/*",
                "dist/*",
                ".git/*",
                "*__pychache__*",
                "README.md"
            ],
            "gui_apps" : {
                "Sandastray" : "main.py"
            },
            "icons" : {
                "Sandastray" : [
                    "icon/icon512.png"
                ]
            },
            "plugins" : [
                "pandagl",
                "p3openal_audio",
            ],
            "platforms" : [
                "manylinux1_x86_64",
                "macosx_10_6_x86_64",
                "win_amd64"
            ],
            "log_filename" : "$USER_APPDATA/sandastray/output.log",
            "log_append" : False
        }
    }
)
