from panda3d.core import Vec3
from direct.stdpy import threading2
from panda3d.core import DirectionalLight
from panda3d.bullet import BulletDebugNode
from panda3d.bullet import BulletWorld

from .models import load_models, place_models, load_place_coins
from .audio import Audio
from .particles import ParticleManager
from .physics import PlayerPhysics
from .character import AnimatedCharacter
from .dunes import Dune
from .clouds import CloudMaker
from .camera import SmoothCamera
from .city import City
from .intro import intro
from .harvest import HarvestingMechanic

class Game():
    def __init__(self):
        self.audio = Audio()
        debug_node = BulletDebugNode('Debug')
        debug_node.show_wireframe(True)
        debug_node.show_constraints(True)
        debug_node.show_bounding_boxes(True)
        debug_node.show_normals(False)
        debug_np = render.attach_new_node(debug_node)
        #debug_np.show()

        self.bullet_world = BulletWorld()
        self.bullet_world.set_gravity(Vec3(0,0,-9.81))
        self.bullet_world.set_debug_node(debug_np.node())

        load_models()
        load_place_coins(self.bullet_world)
        self.cloud_maker = CloudMaker(self.bullet_world)
        self.dune = Dune(self.bullet_world, self.cloud_maker)
        self.player_physics = PlayerPhysics(self.bullet_world)
        self.animated_character = AnimatedCharacter(self.player_physics)
        self.city = City(self.bullet_world)
        self.camera = SmoothCamera(self.player_physics, self.city)
        self.particle_mgr = ParticleManager(self)
        #self.harvesting = HarvestingMechanic(self.cloud_maker, self.city)
        intro()


        self.solar = render.attach_new_node('solar')
        sun = self.solar.attach_new_node(DirectionalLight('sun'))
        sun.node().set_color((1,0.9,0.6,1))
        moon = self.solar.attach_new_node(DirectionalLight('moon'))
        moon.node().set_color((0.6,0.9,1,1))
        moon.set_h(180)
        self.solar.set_hpr(-45,-50,50)
        render.set_light(sun)
        render.set_light(moon)
        render.set_color_scale((1,0.9,0.8,1))

        base.task_mgr.add(self.update_physics)

    def update_physics(self, task):
        def thread_phys():
            self.bullet_world.do_physics(globalClock.get_dt(), 5, 1/160)
            
        threading2._start_new_thread(thread_phys, ())
        
        return task.cont
