import sys, os
from random import choice, uniform
from direct.interval.IntervalGlobal import *

class Audio():
    def __init__(self):
        self.sound = {}
        self.music = {}

        for song in [
            'Lvidbvck',
            '707drums',
            'WindySurfer',
            'Spacetone_TaelLingLin_Loop',
            'Click_Clock_Dance'
        ]:
            self.music[song] = base.loader.load_sfx('assets/music/'+song+'.ogg')

        for sound in [
            'coin',
            'glidewind',
            'normalwind',
            'skywind',
            'sandwind',
            'sandimpactsmash',
            'sandimpactswoosh',
            'sandimpactswoosh2',
            'transform',
        ]:
            self.sound[sound] = base.loader.load_sfx('assets/sound/'+sound+'.wav')
            self.sound[sound].set_volume(0.5)

        for wind in [
            'glidewind',
            'normalwind',
            'skywind',
            'sandwind',
        ]:
            self.sound[wind].set_volume(0)
            self.sound[wind].set_loop(True)
            self.sound[wind].play()
        self.crossfade_a = 0
        self.crossfade_b = 0
        self.sound['normalwind'].set_volume(0.2)
        self.sound['normalwind'].set_play_rate(0.2)
        self.sound['sandwind'].set_play_rate(0.2)

        self.previous_fall_speed = 0
        self.gliding = False

        self.play_songs(0.5)
        base.task_mgr.add(self.update_wind)

    def play_songs(self, vol):
        music_seq = Sequence()
        music_seq.append(SoundInterval(self.music['Click_Clock_Dance'], volume=vol))
        music_seq.append(Wait(1))
        music_seq.append(SoundInterval(self.music['Spacetone_TaelLingLin_Loop'], volume=vol))
        music_seq.append(Wait(1))
        music_seq.append(SoundInterval(self.music['WindySurfer'], volume=vol))
        music_seq.append(Wait(1))
        music_seq.append(SoundInterval(self.music['707drums'], volume=vol))
        music_seq.append(Wait(1))
        music_seq.append(SoundInterval(self.music['Lvidbvck'], volume=vol))
        music_seq.loop()

    def wind(self):
        dt = base.clock.get_dt()
        player = base.game.player_physics
        input_context = base.device_listener.read_context('player')
        speed = abs(player.vehicle_node.node().get_linear_velocity()[1]/75)
        play_speed = min(1,0.2+speed)
        self.sound['sandwind'].set_play_rate(play_speed)
        self.sound['skywind'].set_play_rate(play_speed)

        if player.is_on_ground():
            self.crossfade_a += dt*8
            self.crossfade_b -= dt*2
        else:
            if input_context['glide']:
                self.crossfade_b += dt*2
            else:
                self.crossfade_b -= dt*2
            self.crossfade_a -= dt*8

        self.crossfade_a = min(1,max(self.crossfade_a, 0))
        self.crossfade_b = min(1,max(self.crossfade_b, 0))

        self.sound['sandwind'].set_volume(self.crossfade_a/2)
        self.sound['skywind'].set_volume((1-self.crossfade_a)/4)
        self.sound['glidewind'].set_volume((self.crossfade_b)/4)

        if input_context['glide'] and not self.gliding:
            self.gliding = True
            def play(task):
                self.sound['transform'].play()
            base.task_mgr.do_method_later(0.25, play, 'transform_sound')
        elif not input_context['glide'] and self.gliding:
            self.gliding = False
            self.sound['transform'].play()

    def update_wind(self, task):
        dt = base.clock.get_dt()
        player = base.game.player_physics
        self.wind()
        fall_speed = player.vehicle_node.node().get_linear_velocity()[2]
        ouch = fall_speed - self.previous_fall_speed
        if ouch > 30:
            self.sound['sandimpactsmash'].set_play_rate(1-uniform(0,0.2))
            self.sound['sandimpactsmash'].play()
        elif ouch > 20:
            self.sound['sandimpactswoosh2'].set_play_rate(1-uniform(0,0.2))
            self.sound['sandimpactswoosh2'].play()
        elif ouch > 10:
            self.sound['sandimpactswoosh'].set_play_rate(1-uniform(0,0.1))
            self.sound['sandimpactswoosh'].play()
        elif ouch > 5:
            self.sound['sandimpactswoosh'].set_play_rate(0.5+uniform(-0.1,0.1))
            self.sound['sandimpactswoosh'].play()
        self.previous_fall_speed = fall_speed

        return task.cont
