from panda3d.core import GeomVertexData
from panda3d.core import GeomVertexFormat
from panda3d.core import GeomVertexWriter
from panda3d.core import GeomPoints
from panda3d.core import Geom
from panda3d.core import GeomNode
from panda3d.core import TexGenAttrib
from panda3d.core import TextureStage
from panda3d.core import NodePath


class PointMaker():
    def __init__(self, name):
        self.name = name
        self.scale = 32
        self.new()

    def new(self, scale=32):
        self.scale = scale
        self.point = 0
        self.vdata = GeomVertexData(
            self.name, GeomVertexFormat.get_v3c4(), Geom.UHStatic)
        self.vdata.set_num_rows(1)
        self.vertex = GeomVertexWriter(self.vdata, 'vertex')
        self.color = GeomVertexWriter(self.vdata, 'color')
        self.prim = GeomPoints(Geom.UHStatic)

    def add(self, pos=(0,0,0), color=(1,0.5,0.1,1)):
        self.vertex.add_data3(pos[0],pos[1],pos[2])
        self.color.add_data4(color)
        self.prim.add_vertex(self.point)
        self.point += 1

    def wrap_up(self, sprite=True, perspective=True):
        self.prim.close_primitive()
        geom = Geom(self.vdata)
        geom.add_primitive(self.prim)
        node = GeomNode('point')
        node.add_geom(geom)
        np = NodePath(node)
        if perspective:
            np.set_render_mode_perspective(2)
        if sprite:
            np.set_render_mode_thickness(self.scale/2)
            np.set_tex_gen(TextureStage.getDefault(), TexGenAttrib.MPointSprite)
        np.set_transparency(True)
        return np

    def single(self, color=(1,1,1,1)):
        self.new()
        self.add(color=color)
        return self.wrap_up()
