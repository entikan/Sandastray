from random import choice, randint, uniform
from panda3d.core import CardMaker
from panda3d.core import PNMImage
from panda3d.core import Texture
from panda3d.core import Fog
from panda3d.core import NodePath
from panda3d.core import Vec4

from direct.task import Task

from .points import PointMaker


def red():
    return Vec4(1,0.2,0.2,uniform(0.6,0.9))
def orange():
    return Vec4(1,0.8,0.2,uniform(0.1,0.4))
def purple():
    return Vec4(1,0.2,0.8,uniform(0.6,0.9))


class CloudMaker():
    def __init__(self, bullet_world):
        self.root = render.attach_new_node('cloudfield')
        self.pointmaker = PointMaker('points')

        self.make_textures(6)
        self.bullet_world = bullet_world

        fog = render.attach_new_node(Fog('sand'))
        fog.node().set_color((1,0.6,0.2, 0.5))
        fog.node().set_exp_density(0.0005)
        render.set_fog(fog.node())
        self.root.set_light_off()

    def make_textures(self, amount, res=1024):
        self.textures = []
        spot = PNMImage(res, res)
        spot.render_spot(fg=(1,1,1,1), bg=(0,0,0,0), min_radius=0, max_radius=1)
        for i in range(amount):
            image = PNMImage(res, res)
            image.add_alpha()
            image.perlin_noise_fill(0.6, 0.1, table_size=32, seed=i)
            image.copy_channel(image, 1, 3)
            image.copy_channel(spot, 1, 3)
            image.apply_exponent(1,1,1,-1)
            texture = Texture('cloud_'+str(i))
            texture.load(image)
            self.textures.append(texture)

    def make_cloud(self, color_factory, point_size, radius, points, xo=0, yo=0):
        self.pointmaker.new(point_size)
        for p in range(points):
            x, y = randint(-radius, radius)+xo, randint(-radius, radius)+yo
            result = self.bullet_world.ray_test_closest((x,y,10000),(x,y,-100000))
            pos = result.get_hit_pos()
            pos.z -= point_size/2
            pos.x -= xo; pos.y -= yo
            color = color_factory()
            self.pointmaker.add(pos, color)
        return self.pointmaker.wrap_up()

    def make_dust_clouds(self, amount, radius=1024, x=0, y=0):
        root = render.attach_new_node('clouds')
        sizes = []
        for i in range(amount):
            sizes.append(128+(i*1.2))
        for c in range(amount):
            size = sizes.pop()
            cloud = self.make_cloud(orange, size, radius*2, 128, x, y)
            cloud.set_texture(choice(self.textures))
            cloud.reparent_to(root)
            cloud.set_depth_write(False)
        self.root.flatten_strong()
        def rotate_clouds(task):
            root.set_h(root, 0.001)
            return Task.cont
        base.task_mgr.add(rotate_clouds)

    def make_harvest_cloud(self, point_size, radius, points, xo, yo):
        cloud = self.make_cloud(red, point_size, radius, points, xo, yo)
        cloud.set_texture(choice(self.textures))
        cloud.set_depth_write(False)
        return cloud


if __name__ == "__main__":
    from direct.showbase.ShowBase import ShowBase
    from panda3d.core import DirectionalLight

    base = ShowBase()
    field = CloudField()
    base.run()
