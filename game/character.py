from math import sin
from random import uniform
from direct.actor.Actor import Actor
from direct.interval.IntervalGlobal import *
import random


class AnimatedCharacter():
    def __init__(self, player_physics):
        self.model = Actor(loader.load_model('assets/bam/player.bam'))
        # self.model.set_bin('fixed', 0)
        # self.model.set_depth_test(False)
        # self.model.set_depth_write(False)
        self.model.reparent_to(player_physics.hook)
        self.model.set_pos(0, 0, -0.6)
        self.model.play('glide_to_sail')
        self.last_animation = 'glide_to_sail'
        self.wind = 0
        self.trick_number = 0
        self.flip_tricks()

        # NOTE: PHYSICS IS READ ONLY HERE!
        self.player_physics = player_physics
        self.bullet_world = player_physics.bullet_world

        base.task_mgr.add(self.update)

    def flip_tricks(self):
        board = loader.load_model('assets/bam/board.bam')
        board.reparent_to(self.model)
        board.hide()
        def hide_og_board():
            board.show()
            self.model.find('**/board').hide()
        def show_og_board():
            board.hide()
            self.model.find('**/board').show()
        board_pos = board.get_pos()
        board_pos_adj = board_pos[0], board_pos[1], board_pos[2] - 0.3
        kf_seq = Sequence()
        kickflip = LerpPosHprInterval(board, 0.5, (board_pos_adj), (0, 0, 360), blendType='easeInOut')
        reset = LerpHprInterval(board, 0, (0, 0, 0))
        kick_up = LerpPosInterval(board, 0.2, (board_pos), blendType='easeIn')
        kf_seq.append(Func(hide_og_board))
        kf_seq.append(kickflip)
        kf_seq.append(reset)
        kf_seq.append(kick_up)
        kf_seq.append(Func(show_og_board))
        def kick_it():
            kf_seq.start()
        tf_seq = Sequence()
        treflip = LerpPosHprInterval(board, 0.5, (board_pos_adj), (360, 0, 360), blendType='easeInOut')
        reset = LerpHprInterval(board, 0, (0, 0, 0))
        kick_up = LerpPosInterval(board, 0.2, (board_pos), blendType='easeIn')
        tf_seq.append(Func(hide_og_board))
        tf_seq.append(treflip)
        tf_seq.append(reset)
        tf_seq.append(kick_up)
        tf_seq.append(Func(show_og_board))
        def tre_it():
            tf_seq.start()
        def do_trick():
            self.trick_number = random.randint(0,1)
            if self.trick_number == 0:
                tre_it()
            elif self.trick_number == 1:
                kick_it()
        base.accept('t', do_trick)

    def set_shapekey(self, name, value):
        for char in self.model.find_all_matches('**/+Character'):
            char.node().get_bundle(0).freeze_joint(name, value)

    def play(self, name):
        if not self.last_animation == name:
            self.model.play(name)
            self.last_animation = name

    def update(self, task):
        self.velocity = self.player_physics.vehicle_node.node().get_linear_velocity()
        self.wind += uniform(0,self.player_physics.board_speed/1.2)*base.clock.get_dt()

        # Cape and wing flickering in the wind
        self.set_shapekey('cape_wind',sin(self.wind)/2)
        self.set_shapekey('wing_wind',sin(self.wind)/2)
        self.set_shapekey('clothing_wind',sin(self.wind/2))

        # Cape elongates on player speed
        cape = max(-0.1, min(1, 1 - (self.player_physics.board_speed/80) + (sin(self.wind)/20)))

        if self.velocity.z < 1:
            cape += self.velocity.z/16
        self.set_shapekey('cape_down',cape)


        # Switch between sailing and gliding
        input_context = base.device_listener.read_context('player')
        if input_context['glide']:
            self.play('sail_to_glide')
        else:
            self.play('glide_to_sail')

        return task.cont
