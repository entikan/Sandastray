from random import choice, randint, uniform
from panda3d.bullet import BulletTriangleMesh
from panda3d.bullet import BulletTriangleMeshShape
from panda3d.bullet import BulletRigidBodyNode
from panda3d.bullet import BulletGhostNode
from panda3d.bullet import BulletBoxShape
from panda3d.core import BitMask32, Vec3
from direct.interval.IntervalGlobal import *


def load_models():
    models = base.models = {}
    models['city'] = loader.load_model('assets/bam/city.bam')
    models['player'] = loader.load_model('assets/bam/player.bam')
    props = models['props'] = {}
    for child in loader.load_model('assets/bam/props.bam').get_children():
        props[child.name] = child
        child.detach_node()
        child.clear_transform()

    skate = models['skate'] = {}
    for child in loader.load_model('assets/bam/skatepark.bam').get_children():
        skate[child.name] = child
        child.detach_node()
        child.clear_transform()

def place_models(bullet_world, res, ox, oy):
    root = render.attach_new_node('models')
    root.set_pos(ox, oy, 0)
    half = res/2
    props_body = BulletRigidBodyNode('props')
    def place_random_model(model, scale):
        x, y = randint(-half,half)+ox,randint(-half,half)+oy
        result = bullet_world.ray_test_closest((x, y, 10000),(x,y,-10000))
        pos = result.get_hit_pos()
        pos.z -= uniform(1,2)
        if not pos.is_nan():
            model.copy_to(root)
            model.set_pos(render, pos)
            model.set_scale(scale)
            model.set_h(randint(0,360))

    def bulletize(bullet_world, nodepath):
        nodepath.flatten_strong()
        mesh = BulletTriangleMesh()
        for geom_node in nodepath.find_all_matches('**/+GeomNode'):
            for geom in geom_node.node().get_geoms():
                mesh.add_geom(geom)
        shape = BulletTriangleMeshShape(mesh, dynamic=False)
        props_body.add_shape(shape)
        bullet_world.attach_rigid_body(props_body)

    models = [
        'mountain', 'Grid',
        'building', 'building.001',
        'rock.001', 'rock.002','rock.003','rock.004', 'rock.005','rock.006','rock.007','rock.008','rock.009',
    ]
    for i in range(15):
        place_random_model(base.models['props'][choice(models)], scale=randint(5,7))

    models = [
        'pole', 'pipe', 'looping',
        'halfpipe', 'halfpipe_low', 'spiral', 'quarterpipe', 'ramp',
        'grind', 'funbox_grind', 'funbox', 'frisbee'
    ]
    for i in range(4):
        place_random_model(base.models['skate'][choice(models)], scale=randint(3,5))
    bulletize(bullet_world, root)

def load_place_coins(bullet_world):
    gold_coin = loader.load_model('assets/bam/coin_gold.bam')
    silver_coin = loader.load_model('assets/bam/coin_silver.bam')

    gold_num = 0
    silver_num = 0

    for x in range(5):
        gold_num += 1
        root = render.attach_new_node('gold_coin_' + str(gold_num))
        root.set_pos(randint(-1000, 1000), randint(-1000, 1000), randint(45, 50))
        gold_coin.copy_to(root)
        gold_coin.set_scale(20)
        spin = LerpHprInterval(root, 5, (180, 0, 0)).loop()

        special_shape = BulletBoxShape(Vec3(30, 30, 20))
        body = BulletGhostNode('gold_coin_ghost_' + str(gold_num))
        special_node = root.attach_new_node(body)
        special_node.node().add_shape(special_shape)
        # special_node.set_collide_mask(BitMask32(0x0f))
        bullet_world.attach_ghost(special_node.node())

    for x in range(5):
        silver_num += 1
        root = render.attach_new_node('models_' + str(silver_num))
        root.set_pos(randint(-1000, 1000), randint(-1000, 1000), randint(45, 50))
        silver_coin.copy_to(root)
        silver_coin.set_scale(20)
        spin = LerpHprInterval(root, 5, (180, 0, 0)).loop()

        special_shape = BulletBoxShape(Vec3(30, 30, 20))
        body = BulletGhostNode('silver_coin_ghost_' + str(silver_num))
        special_node = root.attach_new_node(body)
        special_node.node().add_shape(special_shape)
        # special_node.set_collide_mask(BitMask32(0x0f))
        bullet_world.attach_ghost(special_node.node())

