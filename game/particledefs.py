from direct.particles import Particles, ForceGroup
from direct.particles.ParticleEffect import ParticleEffect
from panda3d.core import *
from panda3d.physics import BaseParticleRenderer, BaseParticleEmitter, LinearVectorForce, LinearJitterForce


class ParticleSandTrail(ParticleEffect):
    def __init__(self):
        super().__init__()

        self.reset()
        self.setPos(0.000, 0.000, 0.000)
        self.setHpr(0.000, 0.000, 0.000)
        self.setScale(1.000, 1.000, 1.000)
        p0 = Particles.Particles('particles-1')
        # Particles parameters
        p0.setFactory("PointParticleFactory")
        p0.setRenderer("SpriteParticleRenderer")
        p0.setEmitter("SphereVolumeEmitter")
        p0.setPoolSize(10240)
        p0.setBirthRate(0.0050)
        p0.setLitterSize(10)
        p0.setLitterSpread(0)
        p0.setSystemLifespan(0.0000)
        p0.setLocalVelocityFlag(1)
        p0.setSystemGrowsOlderFlag(0)
        # Factory parameters
        p0.factory.setLifespanBase(0.5000)
        p0.factory.setLifespanSpread(0.0000)
        p0.factory.setMassBase(1.0000)
        p0.factory.setMassSpread(0.0000)
        p0.factory.setTerminalVelocityBase(400.0000)
        p0.factory.setTerminalVelocitySpread(0.0000)
        # Point factory parameters
        # Renderer parameters
        p0.renderer.setAlphaMode(BaseParticleRenderer.PRALPHAUSER)
        p0.renderer.setUserAlpha(1.00)
        # Sprite parameters
        p0.renderer.addTextureFromFile('assets/textures/sandparticle.png')
        p0.renderer.setColor(Vec4(1.00, 1.00, 1.00, 1.00))
        p0.renderer.setXScaleFlag(0)
        p0.renderer.setYScaleFlag(0)
        p0.renderer.setAnimAngleFlag(0)
        p0.renderer.setInitialXScale(1.0000)
        p0.renderer.setFinalXScale(1.0000)
        p0.renderer.setInitialYScale(1.0000)
        p0.renderer.setFinalYScale(1.0000)
        p0.renderer.setNonanimatedTheta(0.0000)
        p0.renderer.setAlphaBlendMethod(BaseParticleRenderer.PPBLENDLINEAR)
        p0.renderer.setAlphaDisable(0)
        # Emitter parameters
        p0.emitter.setEmissionType(BaseParticleEmitter.ETRADIATE)
        p0.emitter.setAmplitude(1.0000)
        p0.emitter.setAmplitudeSpread(0.0000)
        p0.emitter.setOffsetForce(Vec3(0.0000, 10.0000, -1.0000))
        p0.emitter.setExplicitLaunchVector(Vec3(1.0000, 0.0000, 0.0000))
        p0.emitter.setRadiateOrigin(Point3(0.0000, 0.0000, 0.0000))
        # Sphere Volume parameters
        p0.emitter.setRadius(0.75)
        self.addParticles(p0)
        f0 = ForceGroup.ForceGroup('forces')
        # Force parameters
        force0 = LinearVectorForce(Vec3(0.0000, -10.0000, -8.0000), 1.0000, 0)
        force0.setVectorMasks(1, 1, 1)
        force0.setActive(1)
        f0.addForce(force0)
        force1 = LinearJitterForce(30.0000, 0)
        force1.setVectorMasks(1, 0, 1)
        force1.setActive(1)
        f0.addForce(force1)
        self.addForceGroup(f0)



class ParticleSandStorm(ParticleEffect):
    def __init__(self):
        super().__init__()

        self.reset()
        self.setPos(0.000, 0.000, 0.000)
        self.setHpr(0.000, 0.000, 0.000)
        self.setScale(1.000, 1.000, 1.000)
        p0 = Particles.Particles('particles-1')
        # Particles parameters
        p0.setFactory("PointParticleFactory")
        p0.setRenderer("SpriteParticleRenderer")
        p0.setEmitter("BoxEmitter")
        p0.setPoolSize(10240)
        p0.setBirthRate(0.0800)
        p0.setLitterSize(20)
        p0.setLitterSpread(0)
        p0.setSystemLifespan(0.0000)
        p0.setLocalVelocityFlag(1)
        p0.setSystemGrowsOlderFlag(0)
        # Factory parameters
        p0.factory.setLifespanBase(3.0000)
        p0.factory.setLifespanSpread(0.0000)
        p0.factory.setMassBase(1.0000)
        p0.factory.setMassSpread(0.0000)
        p0.factory.setTerminalVelocityBase(400.0000)
        p0.factory.setTerminalVelocitySpread(0.0000)
        # Point factory parameters
        # Renderer parameters
        p0.renderer.setAlphaMode(BaseParticleRenderer.PRALPHAUSER)
        p0.renderer.setUserAlpha(0.1)
        # Sprite parameters
        p0.renderer.addTextureFromFile('assets/textures/sandcloudparticle.png')
        p0.renderer.setColor(Vec4(1.00, 1.00, 1.00, 1.00))
        p0.renderer.setXScaleFlag(1)
        p0.renderer.setYScaleFlag(1)
        p0.renderer.setAnimAngleFlag(0)
        p0.renderer.setInitialXScale(2.0000)
        p0.renderer.setFinalXScale(1.0000)
        p0.renderer.setInitialYScale(2.0000)
        p0.renderer.setFinalYScale(1.0000)
        p0.renderer.setNonanimatedTheta(0.0000)
        p0.renderer.setAlphaBlendMethod(BaseParticleRenderer.PPBLENDLINEAR)
        p0.renderer.setAlphaDisable(0)
        p0.renderer.setColorBlendMode(ColorBlendAttrib.MAdd, ColorBlendAttrib.OIncomingAlpha,
                                      ColorBlendAttrib.OOneMinusIncomingAlpha)
        # Emitter parameters
        p0.emitter.setEmissionType(BaseParticleEmitter.ETRADIATE)
        p0.emitter.setAmplitude(1.0000)
        p0.emitter.setAmplitudeSpread(0.0000)
        p0.emitter.setOffsetForce(Vec3(0.0000, -20.0000, 0.0000))
        p0.emitter.setExplicitLaunchVector(Vec3(1.0000, 0.0000, 0.0000))
        p0.emitter.setRadiateOrigin(Point3(0.0000, 0.0000, 0.0000))
        # Rectangle parameters
        p0.emitter.setMinBound(Point3(-5.0000, 1, -5.0000))
        p0.emitter.setMaxBound(Point3(5.0000, 1, 5.5000))
        self.addParticles(p0)
        f0 = ForceGroup.ForceGroup('forces')
        # Force parameters
        force0 = LinearVectorForce(Vec3(0.0000, -10.0000, 0.0000), 1.0000, 0)
        force0.setVectorMasks(1, 1, 1)
        force0.setActive(1)
        f0.addForce(force0)
        force1 = LinearJitterForce(3.0000, 0)
        force1.setVectorMasks(1, 1, 1)
        force1.setActive(1)
        f0.addForce(force1)
        self.addForceGroup(f0)


class ParticleSandCity(ParticleEffect):
    def __init__(self):
        super().__init__()

        self.reset()
        self.setPos(0.000, 0.000, 0.000)
        self.setHpr(0.000, 0.000, 0.000)
        self.setScale(1.000, 1.000, 1.000)
        p0 = Particles.Particles('particles-1')
        # Particles parameters
        p0.setFactory("PointParticleFactory")
        p0.setRenderer("SpriteParticleRenderer")
        p0.setEmitter("BoxEmitter")
        p0.setPoolSize(1024)
        p0.setBirthRate(0.010)
        p0.setLitterSize(10)
        p0.setLitterSpread(0)
        p0.setSystemLifespan(0.0000)
        p0.setLocalVelocityFlag(1)
        p0.setSystemGrowsOlderFlag(0)
        # Factory parameters
        p0.factory.setLifespanBase(0.5000)
        p0.factory.setLifespanSpread(0.0000)
        p0.factory.setMassBase(1.0000)
        p0.factory.setMassSpread(0.0000)
        p0.factory.setTerminalVelocityBase(10.0000)
        p0.factory.setTerminalVelocitySpread(0.0000)
        # Point factory parameters
        # Renderer parameters
        p0.renderer.setAlphaMode(BaseParticleRenderer.PRALPHAUSER)
        p0.renderer.setUserAlpha(0.50)
        # Sprite parameters
        p0.renderer.addTextureFromFile('assets/textures/sandcloudparticle.png')
        p0.renderer.setColor(Vec4(1.00, 1.00, 1.00, 1.00))
        p0.renderer.setXScaleFlag(1)
        p0.renderer.setYScaleFlag(1)
        p0.renderer.setAnimAngleFlag(0)
        p0.renderer.setInitialXScale(5.0000)
        p0.renderer.setFinalXScale(5.0000)
        p0.renderer.setInitialYScale(5.0000)
        p0.renderer.setFinalYScale(9.0000)
        p0.renderer.setNonanimatedTheta(0.0000)
        p0.renderer.setAlphaBlendMethod(BaseParticleRenderer.PPBLENDLINEAR)
        p0.renderer.setAlphaDisable(0)
        # Emitter parameters
        p0.emitter.setEmissionType(BaseParticleEmitter.ETRADIATE)
        p0.emitter.setAmplitude(1.0000)
        p0.emitter.setAmplitudeSpread(0.0000)
        p0.emitter.setOffsetForce(Vec3(50.0000, 0.0000, -10.0000))
        p0.emitter.setExplicitLaunchVector(Vec3(1.0000, .0000, 0.0000))
        p0.emitter.setRadiateOrigin(Point3(0.0000, 0.0000, 0.0000))
        # Box parameters
        p0.emitter.setMinBound(Point3(-3.0000, -0.5000, -0.5000))
        p0.emitter.setMaxBound(Point3(3.0000, 0.5000, 0.5000))
        self.addParticles(p0)
        f0 = ForceGroup.ForceGroup('forces')
        # Force parameters
        force0 = LinearVectorForce(Vec3(0.0000, 0.0000, -5.0000), 1.0000, 0)
        force0.setVectorMasks(1, 1, 1)
        force0.setActive(0)
        f0.addForce(force0)
        force1 = LinearJitterForce(25.0000, 0)
        force1.setVectorMasks(1, 1, 1)
        force1.setActive(1)
        f0.addForce(force1)
        self.addForceGroup(f0)
