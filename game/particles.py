from direct.particles.ParticleEffect import ParticleEffect
from panda3d.core import Point3

from .particledefs import ParticleSandTrail, ParticleSandStorm, ParticleSandCity
from typing import *

if TYPE_CHECKING:
    from .game import Game


class ParticleManager:
    def __init__(self, game: 'Game'):
        self.game = game
        self.physics = game.player_physics
        self.board_particles: Optional[ParticleEffect] = None
        self.storm_particles: Optional[ParticleEffect] = None
        self.city_particles: Optional[ParticleEffect] = None
        self.setup_board_particles()
        #self.setup_storm_particles()
        self.setup_city_particles()
        base.task_mgr.add(self.update)
        #base.oobe()

    def setup_board_particles(self):
        self.board_particles = ParticleSandTrail()
        self.board_particles.getParticlesList()[0].setBirthRate(100)
        self.board_particles.start(self.physics.hook, render)
        self.board_particles.setZ(-1.5)

    def setup_storm_particles(self):
        self.storm_particles = ParticleSandStorm()
        self.storm_particles.start(base.cam, render)
        self.storm_particles.setY(100)
        self.storm_particles.setZ(-1)

    def setup_city_particles(self):
        self.city_particles = ParticleSandCity()
        self.city_particles.getParticlesList()[0].emitter.setMinBound(Point3(-37.5568, -53.8736, -13.9624))
        self.city_particles.getParticlesList()[0].emitter.setMaxBound(Point3(42.8685, -87.84, 20.4528))
        self.city_particles.start(self.game.city.vehicle_node, render)

    def update(self, task):
        # Update board particles
        if self.board_particles:
            if self.physics.is_on_ground():
                new_rate = -0.001 * self.physics.b_speed + 0.09
                new_rate = max(new_rate, 0.007)
                self.board_particles.getParticlesList()[0].setBirthRate(new_rate)
            else:
                self.board_particles.getParticlesList()[0].setBirthRate(100)

        return task.cont
