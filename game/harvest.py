from random import randint


class HarvestArea():
    def __init__(self, clouds):
        self.clouds = clouds
        self.fuel = 100


class HarvestingMechanic():
    def __init__(self, cloud_maker, city):
        self.root = render.attach_new_node('harvest areas')
        self.cloud_maker = cloud_maker
        self.city = city
        self.harvest_areas = []
        for i in range(16):
            x, y = randint(-1024,1024), randint(-1024,1024)
            cloud = cloud_maker.make_harvest_cloud(point_size=32, radius=64, points=64, xo=x, yo=y)
            cloud.reparent_to(self.root)
            cloud.set_pos(x, y, 0)
            self.harvest_areas.append(HarvestArea(cloud))
        self.fuel = [100,200]
        base.task_mgr.add(self.update)

    def update(self, task):
        for a, area in enumerate(self.harvest_areas):
            if self.city.root.get_distance(area.clouds) < 100:
                self.fuel[0] += area.fuel
                if self.fuel[0] > self.fuel[1]:
                    self.fuel[0] = self.fuel[1]
                area.clouds.detach_node()
                self.harvest_areas.remove(area)
        return task.cont
