from direct.interval.IntervalGlobal import *
from panda3d.core import NodePath
from panda3d.core import TextNode


def intro():
    fish_font = loader.load_font('assets/font/PoorFish/PoorFish-Regular.otf')
    fish_font.set_pixels_per_unit(100)
    fish_font.set_page_size(512, 512)
    fish_font.set_line_height(0.4)

    root = base.aspect2d.attach_new_node('intro root')

    def make_text(text, pos=(0,0,0), scale=0.1, root=None):
        if not root:
            root = NodePath('intro_text')
        text_np = root.attach_new_node(TextNode('text'))
        text_np.node().set_font(fish_font)
        text_np.node().set_text_color((0.5, 0.4, 0.2, 1))
        text_np.node().text = text
        text_np.node().align = 2
        text_np.node().set_shadow(0.02)
        text_np.node().set_shadow_color((0.2,0.1,0.05,1))
        text_np.node().set_small_caps(True)
        text_np.set_pos(pos)
        text_np.set_scale(scale)
        return root

    screens = []
    screens.append(make_text('The Panda Temple\n\nPresents', scale=0.2))
    screens.append(make_text('Powered By PANDA3D', scale=0.2))
    screens.append(make_text('Sandastray', scale=0.3))
    credits = NodePath('a')
    make_text('Created by', scale=0.07, root=credits)
    make_text('\nEntikan\nSimulan\nMaxwell\nSchwarzbaer\nNica',scale=0.2, root=credits)
    screens.append(credits)
    screens.append(make_text('Music By\nTael', scale=0.2))
    screens.append(make_text(''))
    screens.append(make_text('WASD to MOVE\nSPACE to GLIDE', scale=0.07, pos=(0,0,-0.5)))
    screens.append(make_text('T to TRICK\nR to PITCH UP', scale=0.07, pos=(0,0,-0.5)))
    screens.append(make_text('Press Z for CITY\nGet coins to mark area for city.', scale=0.07, pos=(0,0,-0.5)))
    title_sequence = Sequence(Wait(3))
    for screen in screens:
        title_sequence.append(
            Sequence(
                Func(screen.reparent_to, root),
                screen.colorScaleInterval(1, (1, 1, 1, 1), (1, 1, 1, 0)),
                Wait(3),
                screen.colorScaleInterval(1, (1, 1, 1, 0)),
                Func(screen.detach_node),
                Wait(1),
            )
        )


    title_sequence.start()
