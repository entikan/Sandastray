import sys

from direct.showbase.ShowBase import ShowBase
from panda3d.core import load_prc_file_data, Filename
from panda3d.core import PNMImage
from panda3d.core import TextNode
from keybindings.device_listener import add_device_listener
from keybindings.device_listener import SinglePlayerAssigner
from direct.interval.IntervalGlobal import *

from game.game import Game

load_prc_file_data("",
"""
window-title Sandastray
win-size 1280 720
framebuffer-srgb true
textures-power-2 none
text-minfilter nearest
text-magfilter nearest
text-scale-factor 1
cull-bin true
""")

class Base(ShowBase):
    def __init__(self):
        ShowBase.__init__(self)
        base.camLens.set_near_far(0.1, 900000)
        # Have to make an explicit import here in order for packaging to pick them up.
        from direct.particles import ParticleManagerGlobal
        from direct.showbase import PhysicsManagerGlobal
        self.enableParticles()
        add_device_listener(assigner=SinglePlayerAssigner())
        self.win.set_clear_color((0.2,0.7,1,1))
        self.game = Game()
        self.clock = globalClock ## FIXME: Remove after release of Panda3D 1.11
        self.screen_cap_num = 0

        self.screen_text()
        
        seq = Sequence()
        seq.append(Wait(40))
        seq.append(Func(self.toggle_screen_text))
        seq.start()
        
        base.accept('tab', self.toggle_screen_text)
        
    def toggle_screen_text(self):
        controller_info = self.aspect2d.find('**/controller_info')
        money_info = self.aspect2d.find('**/money_info')
        success_info = self.aspect2d.find('**/success_info')
        
        if controller_info.is_hidden():
            controller_info.show()
            money_info.show()
            success_info.show()
            
        else:
            controller_info.hide()
            money_info.hide()
            success_info.hide()

    def trigger_screencaps(self):
        self.frame_num += 1
        if self.frame_num == 500:
            self.make_screenshot()
            self.frame_num = 0

    def make_screenshot(self):
        base.graphicsEngine.render_frame()
        full = PNMImage()
        base.win.get_screenshot(full)
        small = PNMImage(500, 300)
        small.gaussian_filter_from(1, full)
        small.write(Filename('screen_cap_' + str(self.screen_cap_num) + '.jpg'))
        self.screen_cap_num += 1

    def screen_text(self):
        text_1 = TextNode('controller_info')
        fish_font = loader.load_font('assets/font/PoorFish/PoorFish-Regular.otf')
        fish_font.set_pixels_per_unit(100)
        fish_font.set_page_size(512, 512)
        fish_font.set_line_height(0.8)
        text_1.set_font(fish_font)
        # text_1.set_shadow(0.05)
        text_1.set_text_color((0.5, 0.3, 0.3, 1))
        text_1.set_text('Move: WSAD' + '\n\n' + 'Glide: Space (hold)' + '\n\n' + 'Flight Pitch Up: R' + '\n\n' + 'Pilot City Ship: Z (toggle)' + '\n\n' + 'Perform Trick: T' + '\n\n' + 'Reset Player: F2' + '\n\n' + 'Toggle Info: Tab')
        text_1_node = self.aspect2d.attach_new_node(text_1)
        text_1_node.set_scale(0.07)
        text_1_node.set_pos(-1.5, 0, 0.9)
        text_1.set_small_caps(True)
        text_1_node.hide()

        text_2 = TextNode('money_info')
        text_2.set_font(fish_font)
        # text_2.set_shadow(0.05)
        text_2.set_text_color((0.5, 0.3, 0.3, 1))
        text_2.set_text('Money: 0 credits')
        text_2_node = self.aspect2d.attach_new_node(text_2)
        text_2_node.set_scale(0.07)
        text_2_node.set_pos(1, 0, 0.9)
        text_2.set_small_caps(True)
        text_2_node.hide()

        text_3 = TextNode('success_info')
        text_3.set_font(fish_font)
        # text_3.set_shadow(0.05)
        text_3.set_text_color((0.5, 0.3, 0.3, 1))
        text_3.set_text('Fly through the coins to mark resource areas' + '\n\n' + 'for your city ship and earn credits.')
        text_3_node = self.aspect2d.attach_new_node(text_3)
        text_3_node.set_scale(0.07)
        text_3_node.set_pos(-0.6, 0, 0.9)
        text_3.set_small_caps(True)
        text_3_node.hide()

if __name__ == "__main__":
    base = Base()
    base.accept('escape', sys.exit)
    base.run()
